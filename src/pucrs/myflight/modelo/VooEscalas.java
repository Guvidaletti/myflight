package pucrs.myflight.modelo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class VooEscalas extends Voo {

    private ArrayList<Rota> rotas;

    public VooEscalas(Rota rota, LocalDateTime datahora) {
        super(rota, datahora);
        rotas = new ArrayList<>();
        rotas.add(rota);
    }

    public void adicionarRota(Rota nova) {
        rotas.add(nova);
    }

    public int getTotalRotas() {
        return rotas.size();
    }

    public ArrayList<Rota> getRotas() {
        return new ArrayList<>(rotas);
    }

    public Duration getDuration()
    {
        double tempo=0;
        double dist=0;
        for (Rota r: rotas)
        {
            dist += r.getOrigem().getLocal().distancia(r.getDestino().getLocal());
        }
        tempo += (dist / 805.0)*60.0;
        tempo += (getTotalRotas()*30);
        return Duration.ofMinutes(Math.round(tempo));
    }
    public double getDist()
    {
        double dist=0;
        for (Rota r: rotas)
        {
            dist += r.getOrigem().getLocal().distancia(r.getDestino().getLocal());
        }
        return Math.round(dist);
    }

    @Override
    public String toString() {
        String aux = super.toString();
        for(Rota r: rotas)
            aux += "\n   " + r;
        return aux;
    }

}
