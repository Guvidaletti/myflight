package pucrs.myflight.modelo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GerenciadorRotas
{
    private ArrayList<Rota> rotas;
    public GerenciadorRotas()
    {
        rotas = new ArrayList<>();
    }

    public void adicionar(Rota rota) {rotas.add(rota);}

    public ArrayList<Rota> listarTodas()
    {
    ArrayList<Rota> lista = new ArrayList<>();
    for(Rota rota: rotas)
        lista.add(rota);
        return lista;
    }

    public ArrayList<Rota> buscarPorOrigem(Aeroporto orig)
    {
        ArrayList<Rota> lista = new ArrayList<>();
        for(Rota rota: rotas) {
            if(orig.equals(rota.getOrigem())){
                lista.add(rota);
            }
        }
        return lista;
    }

    //public void ordenaCia(){Collections.sort(rotas);}

    public void ordenaCia(){rotas.sort((Rota r1,Rota r2) -> r1.getCia().getNome().compareTo(r2.getCia().getNome()));}
    public void ordenaOrigem(){rotas.sort((Rota r1,Rota r2) -> r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome()));}
    public void ordenaOrigemCia()
    {
        rotas.sort((Rota r1,Rota r2) -> {
            int result = r1.getOrigem().getNome().compareTo(r2.getOrigem().getNome());
            if(result!=0) return result;
            return r1.getCia().getNome().compareTo(r2.getCia().getNome());
        });
    }

}
