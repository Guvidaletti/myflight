package pucrs.myflight.modelo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class GerenciadorAeroportos
{
    private Map<String, Aeroporto> aeroportos;

    public GerenciadorAeroportos()
    {
        this.aeroportos = new TreeMap<>();
    }

    public void adicionar(Aeroporto aero)
    {
        aeroportos.put(aero.getCodigo(),aero);
    }

    public ArrayList<Aeroporto> listarTodos()
    {
        return new ArrayList<>(aeroportos.values());
    }

    public Aeroporto buscarCodigo(String cod)
    {
        return aeroportos.get(cod);
    }
}
