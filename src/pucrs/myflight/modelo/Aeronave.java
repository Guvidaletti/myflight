package pucrs.myflight.modelo;

public class Aeronave implements Imprimivel, Contavel, Comparable<Aeronave>{
	private static int total = 0;
	private String codigo;
	private String descricao;
	private int capacidade;
	
	public Aeronave(String codigo, String descricao, int cap) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.capacidade = cap;
		total++;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public int getCapacidade() {
		return capacidade;
	}

	public String toString()
	{
		return "Código: " + codigo + "\nDescrição: " + descricao +"\nCapacidade: " + capacidade + "\n";
	}

	@Override
	public void imprimir() {
		System.out.println(codigo + " - " + descricao);
	}

	@Override
	public int totalObjetos() {
		return total;
	}

	@Override
	public int compareTo(Aeronave outra) {return descricao.compareTo(outra.descricao);
	}
}
