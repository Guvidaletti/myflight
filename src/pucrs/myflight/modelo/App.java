package pucrs.myflight.modelo;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class App {
	public static GerenciadorAeroportos gerAero;

	public static void main(String[] args) {
		System.out.println("App iniciado!");

		//LocalDateTime datahora = LocalDateTime.of(20,3,14,22,20);
		GerenciadorAeronaves gerAvioes = new GerenciadorAeronaves();
		//GerenciadorAeroportos gerAeroportos = new GerenciadorAeroportos();
		gerAero = new GerenciadorAeroportos();
		GerenciadorCias gerCias = new GerenciadorCias();
		GerenciadorVoos gerVoos = new GerenciadorVoos();
		GerenciadorRotas gerRotas = new GerenciadorRotas();

		Aeronave av1 = new Aeronave("733","Boeing 737-300",140);
		Aeronave av2 = new Aeronave("73G","Boeing 737-700",126);
		Aeronave av3 = new Aeronave("380","Airbus Industrie A380",644);
		Aeronave av4 = new Aeronave("764","Boeing 767-400",304);
		Aeronave av5 = new Aeronave("332","Boeing 767-666",305);
		Aeronave av6 = new Aeronave("320","Boeing 767-999",310);
		Aeronave av7 = new Aeronave("738","Boeing 767-738",450);
		System.out.println("Total aeronaves: " + av1.totalObjetos());

		gerAvioes.adicionar(av1);
		gerAvioes.adicionar(av2);
		gerAvioes.adicionar(av3);
		gerAvioes.adicionar(av4);
		gerAvioes.adicionar(av5);
		gerAvioes.adicionar(av6);
		gerAvioes.adicionar(av7);

        //gerAvioes.ordenarDescricao();
        gerAvioes.ordenarCodigo();
        for(Aeronave a : gerAvioes.listarTodas())
        {
            System.out.println(a);
        }

		Aeroporto poa = new Aeroporto("POA","Salgado Filho Intl Apt", new Geo(-29.9939, -51.1711));
		Aeroporto gru = new Aeroporto("GRU","Sao Paulo Guarulhos Intl Apt", new Geo(-23.4356 , -46.4731));
		Aeroporto lis = new Aeroporto("LIS","Lisbon Airport", new Geo(38.7742, -9.1342 ));
		Aeroporto mia = new Aeroporto("MIA","Miami International Apt", new Geo(25.7933, -80.2906));
		Aeroporto gig = new Aeroporto("GIG","Rio de Janeiro International Apt", new Geo(-24.7933, -47.2906));

		gerAero.adicionar(poa);
		gerAero.adicionar(gru);
		gerAero.adicionar(lis);
		gerAero.adicionar(mia);
		gerAero.adicionar(gig);

		System.out.println("Ordenação aeroportos: ");
		for(Aeroporto a: gerAero.listarTodos())
		{
			System.out.println(a);
		}

			try{
			gerCias.carregaDados("airlines");
			}catch (IOException e){
				System.out.println("Não foi possível ler airlines.dat");
				System.exit(1);
			}
		/**
		CiaAerea latam = new CiaAerea("JJ","LATAM Linhas Aereas");
		CiaAerea gol = new CiaAerea("G3","Gol Linhas Aereas SA");
		CiaAerea tap = new CiaAerea("TP","TAP Portugal");
		CiaAerea azul = new CiaAerea("AD","Azul Linhas Aereas");
		CiaAerea american1 = new CiaAerea("AR","American Airlines");

		gerCias.adicionar(latam);
		gerCias.adicionar(gol);
		gerCias.adicionar(tap);
		gerCias.adicionar(azul);
		gerCias.adicionar(american1);*/

		LocalDateTime manhacedo = LocalDateTime.of(2018, 3, 29, 8, 0);
		LocalDateTime manhameio = LocalDateTime.of(2018, 4, 4, 10, 0);
		LocalDateTime tardecedo = LocalDateTime.of(2018, 4, 4, 14, 30);
		LocalDateTime tardetarde = LocalDateTime.of(2018, 4, 5, 17, 30);

		Duration curto = Duration.ofMinutes(90);
		Duration longo1 = Duration.ofHours(12);
		Duration longo2 = Duration.ofHours(14);

		Rota poagru = new Rota(gerCias.buscarCodigo("LOT"), poa, gru, av1);
		Rota r2 = new Rota(gerCias.buscarCodigo("LW"), gru, poa, av7);
		Rota grulis = new Rota(gerCias.buscarCodigo("LV"), gru, lis, av3);
		Rota r4 = new Rota(gerCias.buscarCodigo("LT"), gru, gig, av6);
		Rota r5 = new Rota(gerCias.buscarCodigo("LS"), poa, gig, av1);
		Rota r6 = new Rota(gerCias.buscarCodigo("LR"), poa, mia, av2);

		gerRotas.adicionar(poagru);
		gerRotas.adicionar(r2);
		gerRotas.adicionar(grulis);
		gerRotas.adicionar(r4);
		gerRotas.adicionar(r5);
		gerRotas.adicionar(r6);

		gerRotas.ordenaCia();
		System.out.println("Ordenação das rotas por Companhias: ");
		for(Rota r: gerRotas.listarTodas())
		{
			System.out.println(r);
		}
		System.out.println();
		//Voo v1 = new Voo(poagru ,LocalDateTime.of(2016, 8, 10, 8, 0), Duration.ofMinutes(90));
		//Voo v2 = new Voo(r5 ,LocalDateTime.of(2016, 8, 10, 15, 0), Duration.ofMinutes(120));
		//Voo v3 = new Voo(r6 ,LocalDateTime.of(2016, 8, 15, 12, 0), Duration.ofMinutes(120));
		//Voo v4 = new Voo(r2, LocalDateTime.of(2016, 8,16,9,00), Duration.ofHours(14));
		//Voo v5 = new Voo(r5, Duration.ofMinutes(120));

		//gerVoos.adicionar(v1);
		//gerVoos.adicionar(v2);
		//gerVoos.adicionar(v3);
		//gerVoos.adicionar(v4);
		//gerVoos.adicionar(v5);


		VooEscalas vooEsc = new VooEscalas(poagru,
				manhacedo);
		vooEsc.adicionarRota(grulis);

		gerVoos.adicionar(vooEsc);

		// O toString vai usar o método implementado em VooEscalas, mas reutilizando (reuso) o método original de Voo
		System.out.println("VOO ESCALAS: ");
		//gerVoos.ordenarDataHoraDuracao();
		System.out.println("Todos os vôos:\n");
		for(Voo v: gerVoos.listarTodos())
		{
			if(v instanceof VooEscalas) {
				System.out.println(">>> Vôo com escalas!");
				VooEscalas vaux = (VooEscalas) v;
				System.out.println("Escalas: "+vaux.getTotalRotas());
			}
			System.out.println(v);
		}

		/**BUSCA POR PORTO ALEGRE
        ArrayList<Voo> busca = new ArrayList<>();
        //busca = gerVoos.buscarOrigem("POA");
        int tamanho = busca.size();
        System.out.println("busca por 'POA': " + tamanho);
		*/

		/**BUSCA POR COMPANHIA
		CiaAerea aux = gerCias.buscarCodigo("G3");
		if(aux!=null)
			System.out.println(aux.getCodigo() + ": " +aux.getNome());
		else
			System.out.println("Código inexistente!");

		for(CiaAerea cia: gerCias.listarTodas())
		{
			System.out.println("Cod: " + cia.getCodigo());
		}*/
	}
}