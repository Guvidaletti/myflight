package pucrs.myflight.modelo;

import java.time.LocalDateTime;

public class App2 {
    public static GerenciadorAeroportos gerAero;
    public static void main(String[] args) {
        gerAero = new GerenciadorAeroportos();
        GerenciadorCias gerCias = new GerenciadorCias();
        GerenciadorVoos gerVoos = new GerenciadorVoos();
        GerenciadorRotas gerRotas = new GerenciadorRotas();
        Aeroporto poa = new Aeroporto("POA","Salgado Filho Intl Apt", new Geo(-29.9939, -51.1711));
        Aeroporto gru = new Aeroporto("GRU","Sao Paulo Guarulhos Intl Apt", new Geo(-23.4356 , -46.4731));
        Aeroporto lis = new Aeroporto("LIS","Lisbon Airport", new Geo(38.7742, -9.1342 ));
        gerAero.adicionar(poa);
        gerAero.adicionar(gru);
        CiaAerea latam = new CiaAerea("JJ","LATAM Linhas Aereas");
        Aeronave av1 = new Aeronave("733","Boeing 737-300",140);
        Rota poagru = new Rota(latam, poa, gru, av1);
        Rota grulis = new Rota(latam, gru, lis, av1);
        LocalDateTime manhacedo = LocalDateTime.of(2018, 3, 29, 8, 0);

        VooDireto vooDir = new VooDireto(poagru,manhacedo);
        VooEscalas vooEsc = new VooEscalas(poagru,
                manhacedo);
        vooEsc.adicionarRota(poagru);

        gerVoos.adicionar(vooDir);
        System.out.println("APP2");
        System.out.println("VOO: " + vooDir);
        //System.out.println("Distancia: " + Math.round(vooDir.getRotas().get(0).getOrigem().getLocal().distancia(vooDir.getRotas().get(0).getDestino().getLocal())) + " Km");
        System.out.println("Distancia: " + Math.round(vooDir.getDist()) + " Km");
        System.out.println("Tempo de Viagem: " + vooDir.getDuration());
        System.out.println("Se fosse Voo ESCALAS!!!");
        System.out.println("VOOESC: " + vooEsc);
        System.out.println("ESCALAS : " + vooEsc.getTotalRotas());
        System.out.println("Distancia: " + Math.round(vooEsc.getDist()) + " Km");
        System.out.println("Tempo de Viagem: " + vooEsc.getDuration());
    }
}
