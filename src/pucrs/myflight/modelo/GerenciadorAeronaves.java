package pucrs.myflight.modelo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GerenciadorAeronaves {
    private ArrayList<Aeronave> avioes;
    public GerenciadorAeronaves()
    {
        avioes = new ArrayList<>();
    }

    public void adicionar(Aeronave aviao)
    {
        avioes.add(aviao);
    }

    public ArrayList<Aeronave> listarTodas()
    {
        ArrayList<Aeronave> lista = new ArrayList<>();
        for(Aeronave aeronave: avioes)
            lista.add(aeronave);
        return lista;
    }

    public Aeronave buscarPorCodigo(String cod)
    {
        for(Aeronave aeronave: avioes)
        {
            if(cod.equals(aeronave.getCodigo()))
                return aeronave;
        }
        return null;
    }
    public void ordenarDescricao()
    {
        //Collections.sort(avioes);
        //avioes.sort( (Aeronave a1,Aeronave a2) -> a1.getDescricao().compareTo(a2.getDescricao()) );
        //avioes.sort(Comparator.comparing(a -> a.getDescricao()));
        avioes.sort(Comparator.comparing(Aeronave::getDescricao));
    }
    public void ordenarCodigoDescricao()
    {
        avioes.sort(Comparator.comparing(Aeronave::getCapacidade).thenComparing(Aeronave::getDescricao));
    }
    public void ordenarCodigo()
    {
        //Collections.sort(avioes);
        avioes.sort( (Aeronave a1,Aeronave a2) -> a1.getCodigo().compareTo(a2.getCodigo()) );
        //avioes.sort((Aeronave a1, Aeronave a2) -> 
        //{
        //a1.getCodigo().compareTo(a2.getCodigo())
        //});
    }

}
