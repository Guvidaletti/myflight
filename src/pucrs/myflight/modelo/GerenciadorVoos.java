package pucrs.myflight.modelo;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Comparator;

//import java.time.LocalDateTime;
public class GerenciadorVoos
{
    private ArrayList<Voo> viagens;

    public GerenciadorVoos() //construtor
    {
        viagens = new ArrayList<>();
    }

    public void adicionar(Voo voo)
    {
        viagens.add(voo);
    }

    public ArrayList<Voo> listarTodos()
    {
        ArrayList<Voo> lista = new ArrayList<>();
        for(Voo voo: viagens)
            lista.add(voo);

        return lista;
    }

    public ArrayList<Voo> buscarData(LocalDate data)
    {
        ArrayList<Voo> lista = new ArrayList<>();
        for(Voo voo: viagens)
        {
            if(data.equals(voo.getDatahora()))
                lista.add(voo);
        }
        return lista;
    }

    /** METODO DESATIVADO POR VOO SER UMA CLASSE ABSTRATA
    public ArrayList<Voo> buscarOrigem(String cod){
        ArrayList<Voo> lista = new ArrayList<>();
        for(Voo voo: viagens){
            if(voo.getRota().getOrigem().getCodigo().equals(cod))
            {
                lista.add(voo);
            }
        }
        return lista;
    }
     */

    public ArrayList<Voo> getViagens() {
        return viagens;
    }

    public ArrayList<Voo> buscarPorHora(LocalDateTime hi, LocalDateTime hf)
    {
        ArrayList<Voo> lista = new ArrayList<>();
        for(Voo voo: viagens)
            if(voo.getDatahora().isAfter(hi) && voo.getDatahora().isBefore(hf))
                lista.add(voo);
        return lista;
    }
    public void ordenarDataHora()
    {
        viagens.sort(Comparator.comparing(Voo::getDatahora));
    }

    public void ordenarDataHoraDuracao()
    {
        viagens.sort(Comparator.comparing(Voo::getDatahora).thenComparing(Voo::getDuration));}
}