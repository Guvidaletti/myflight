package pucrs.myflight.modelo;

import java.io.IOException;

public class App3 {
    public static void main(String[] args) {
        GerenciadorCias gerCias = new GerenciadorCias();
        try {
            gerCias.carregaDados("airlines.dat");
        } catch (IOException e){
            System.out.println("arquivo inexistente");
        }
        for(CiaAerea a: gerCias.listarTodas()) {
            System.out.println(a);
            System.exit(1);
        }
    }
}
