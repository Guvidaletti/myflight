package pucrs.myflight.modelo;
import java.io.IOException;
import java.util.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class GerenciadorCias {
	//private ArrayList<CiaAerea> empresas;
	private Map<String, CiaAerea> empresas;

	public GerenciadorCias() {
		//this.empresas = new ArrayList<>();
		//this.empresas = new HashMap<>();
		this.empresas = new TreeMap<>();
		//this.empresas = new LinkedHashMap<>();
	}
	public void adicionar(CiaAerea cia1){
		empresas.put(cia1.getCodigo(),cia1);
	}

	public CiaAerea buscarCodigo(String cod) {
		return empresas.get(cod);

	}

	public void carregaDados(String nomeArq)throws IOException
	{
		Path path2 = Paths.get(nomeArq);
		try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabeçalho
			while (sc.hasNext()) {
				String cod = sc.next();
				String nome = sc.next();
				CiaAerea nova = new CiaAerea(cod,nome);
				adicionar(nova);
			}
		}
	}

	public ArrayList<CiaAerea> listarTodas()
	{
		return new ArrayList<>(empresas.values());
	}
}

