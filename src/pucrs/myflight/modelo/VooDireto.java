package pucrs.myflight.modelo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class VooDireto extends Voo {
    //private Rota rota;
    private ArrayList<Rota> rotas;
    private Duration duracion;
    private double dist;
    public VooDireto(Rota rota, LocalDateTime dh)
    {
        super(rota, dh);
        dist = rota.getOrigem().getLocal().distancia(rota.getDestino().getLocal()); // em km
        rotas = new ArrayList<>();
        rotas.add(rota);

    }
    public ArrayList<Rota> getRotas() {
        return new ArrayList<>(rotas);
    }

    public Duration getDuration()
    {
        //1 meter/second = 3.6 kilometer/hour
        //805 km/h * 1000m / 3600s -> M/S
        double tempo = (dist / 805)*60.0;
        tempo += 30;
        return Duration.ofMinutes(Math.round(tempo));
    }
    public double getDist()
    {
        double dist=0;
        for (Rota r: rotas)
        {
            dist += r.getOrigem().getLocal().distancia(r.getDestino().getLocal());
        }
        return Math.round(dist);
    }

    @Override
    public String toString() {
        String aux = super.toString();
        for(Rota r: rotas)
            aux += "\n   " + r;
        return aux;
    }
}
